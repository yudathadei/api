<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\PasswordReset;
use Hash;

class PasswordResetController extends Controller
{
    //create
    public function create(Request $request){

        $request->validate([
            'email'=>'required|string|email'
        ]);
        
        $user=User::where('email',$request->email)->first();

        if(!$user){

             $response =['message','We can not find user with this email'];

             return response()->json($response,404);
        }


        $passwordReset=PasswordReset::updateOrCreate(
            ['email'=> $user->email],
            ['email' => $user->email,
            'token'=> Str::random(60)
            ]
        );

        if($user && $passwordReset){

            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );

            $response=['massage','We have emailed your password reset link '];

            return response()->json($response,200);
        }
    }
    //find
    public function find($token){

        $passwordReset=PasswordReset::where('token',$token)->first();

        if(!$passwordReset){

            $response=['message','This password reste token is invalid'];

            return response()->json($response,404);
        }

        if(Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()){

            $response=['message','This password reste token is invalid'];

            return response()->json($response,404);
        }

        return response()->json($passwordReset);
    }
    //reset
    public function reset(Request $request){
        
        $request->validate([
            'email'=>'required|string|email',
            'password'=>'required|string|min:6',
            'token'=>'required|string'
        ]);

        $passwordReset=PasswordReset::where([
            ['email',$request->email],
            ['token',$request->token]
            ])->first();

        if(!$passwordReset){

            $response=['message','This password reste token is invalid'];

            return response()->json($response,404);
        }

        $user=User::where('email',$passwordReset->email)->first();

        if(!$user){

            $response=['message','we can not find user with this email account !'];

            return response()->json($response,404);
        }

        $request['password']=Hash::make($request['password']);

        $user->password = $request->password;
        $user->save();

        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess($passwordReset));

        return response()->json($user);
    }
}
