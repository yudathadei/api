<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Notifications\RegisterActivate;
use Hash;
use App\User;
use Avatar;
use Storage;
use Auth;

class AuthController extends Controller
{
    //register
    public function register(Request $request){
        
        $validator = Validator::make($request->all(),[
            'name'=> 'required|string',
            'telephone' =>'required|string|unique:users',
            'email'=>'required|string|unique:users',
            'password'=>'required|string|min:6|confirmed'
        ]);

        if(!$validator){
            $response=['error'=>$validator->errors()];
            return response()->json($response,422);
        }

        $request['password']=Hash::make($request['password']);
        $user= new User([
            'name' => $request->name,
            'email' => $request->email,
            'telephone' => $request->telephone,
            'password' => $request->password,
            'activation_token' => Str::random(60)
        ]);

        $user->save();

        $avatar= Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/'.$user->name.'/avatar.png',(string)$avatar);
        $user->notify(new registerActivate($user));

        $response=['message','User created succsessfully'];


        Return response()->json($response,200);

    }
    //login
    public function login(Request $request){

        $request->validate([

            'email'=>'required|string|email',
            'password'=>'required|string',
            'remember_me'=>'boolean'
        ]);

        $credentials=request(['email','password']);
        $credentials['active'] =1;
        $credentials['deleted_at']=null;

        if(!Auth::attempt($credentials)){
            $response=['maessage','Unauthorized'];
            return response()->json($response,401);
        }

        $user =$request->user();
        $tokenResult=$user->createToken('Person Access Token');
        $token=$tokenResult->token;

        if($request->remember_me){

            $token->expires_at = Carbon::now()->addWeeks(2);
        }

        $token->save();

        $response=[
            'access_token'=>$tokenResult->accessToken,
            'token_type'=>'Bearer',
            'expires_at'=>Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ];

        return response()->json($response);

    
    }
    //logout
    public function logout(Request $request){

        $request->user()->token()->revoke();
        $response=['message','Successfully logged out'];

        return response()->json($response,200);

    }
    //user
    public function user(Request $request){

        return response()->json($request->user());
    }
    //RegisterActivate
    public function registerActivate($token){

        $user= User::where('activation_token',$token)->first();

        if(!$user){

            $response=['error','Invalid activation key'];

            return response()->json($response,404);
        }

        $user->active = true;
        $user->activation_token='';
        $user->save();

        return $user;
    }
}
