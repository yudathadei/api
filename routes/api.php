<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix'=> 'auth'],function (){
    Route::post('login','AuthController@login');
    Route::post('register','AuthController@register');
    Route::get('register/activate/{token}','AuthController@registerActivate');
});

// Route::post('login','AuthController@login');
// Route::post('register','AuthController@register');


Route::group(['middleware'=>'auth:api'],function (){
    Route::get('user','AuthController@user');
    Route::get('logout','AuthController@logout');
});

Route::group([
    'namespace'=>'Auth',
    'middleware'=>'api',
    'prefix'=>'password'
],function (){

    Route::post('create','PasswordResetController@create');
    Route::get('find/{token}','PasswordResetController@find');
    Route::post('reset','PasswordResetController@reset');

});